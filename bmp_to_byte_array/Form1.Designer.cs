﻿namespace bmp_to_byte_array
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.convertToByte = new System.Windows.Forms.Button();
            this.byteBox = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.chooseBitmapBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // convertToByte
            // 
            this.convertToByte.Location = new System.Drawing.Point(145, 11);
            this.convertToByte.Name = "convertToByte";
            this.convertToByte.Size = new System.Drawing.Size(75, 23);
            this.convertToByte.TabIndex = 0;
            this.convertToByte.Text = "Convert";
            this.convertToByte.UseVisualStyleBackColor = true;
            this.convertToByte.Click += new System.EventHandler(this.convertToByte_Click);
            // 
            // byteBox
            // 
            this.byteBox.Location = new System.Drawing.Point(12, 41);
            this.byteBox.Name = "byteBox";
            this.byteBox.Size = new System.Drawing.Size(728, 467);
            this.byteBox.TabIndex = 1;
            this.byteBox.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            //this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // chooseBitmapBtn
            // 
            this.chooseBitmapBtn.Location = new System.Drawing.Point(12, 11);
            this.chooseBitmapBtn.Name = "chooseBitmapBtn";
            this.chooseBitmapBtn.Size = new System.Drawing.Size(93, 23);
            this.chooseBitmapBtn.TabIndex = 2;
            this.chooseBitmapBtn.Text = "Choose Bitmap";
            this.chooseBitmapBtn.UseVisualStyleBackColor = true;
            this.chooseBitmapBtn.Click += new System.EventHandler(this.chooseBitmapBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 520);
            this.Controls.Add(this.chooseBitmapBtn);
            this.Controls.Add(this.byteBox);
            this.Controls.Add(this.convertToByte);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button convertToByte;
        private System.Windows.Forms.RichTextBox byteBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button chooseBitmapBtn;
    }
}

